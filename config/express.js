const express = require('express')
      ,app = express()
      ,consign = require('consign')
      ,bodyParser = require('body-parser')
      ,morgan = require('morgan')
      ,logger = require('../app/services/logger');

module.exports = function() {

  app.use(morgan("common", {
    stream: {
      write: function(mensagem) {
        logger.info(mensagem);
      }
    }
  }));

  app.use(bodyParser.json());

  consign({cwd: 'app'})
    .include('routes')
    .then('persistencia')
    .then('services')
    .into(app);

  return app;
}