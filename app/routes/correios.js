module.exports = function(app) {

  app.post('/correios/calculaprazo', function(req, res) {
  
    let dados = req.body;
    let correiosSOAPClient = new app.services.correiosSOAPClient();

    correiosSOAPClient.calculaPrazo(dados, function(err, results) {

      if(err) {
        console.log(err);
        res.status(500).send(err);
        return;
      }

      res.json(results);
    });
  });
}