const fs = require('fs');

module.exports = function(app) {

  app.post('/upload/imagem', function(req, res) {

    console.log('Recebendo imagem');

    let filename = req.headers.filename; //Passado no Header da requisição

    req.pipe(fs.createWriteStream(`files/${filename}${Date.now()}.jpg`)) //A imagem sera gravada dentro da pasta files
       .on('finish', function() {
         console.log('arquivo escrito');
         res.status(201).send('Ok');
       });
  });
}
//curl http://localhost:3000/upload/imagem --data-binary @app/utils/cachoeira.jpg -H "Content-type: application/octet-stream" -v -H "filename: img"