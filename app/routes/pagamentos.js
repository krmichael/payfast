const {
  check,
  validationResult
} = require('express-validator/check');

let logger = require('../services/logger');
let html = `<div>
  <h2 style="text-align:center;">Routers</h2>
  <ul>
    <li>/pagamentos (Todos pagamentos)</li>
    <li>/pagamentos/pagamento [pagamento] (POST pagamento)</li>
    <li>/pagamentos/pagamento/id (GET, UPDATE and DELETE)</li>
  </ul>
</div>`;

module.exports = function (app) {

  app.get('/', function(req, res) {
    res.send(html);
  });

  app.get('/pagamentos', function (req, res) {
    
    let pagamentoDao = new app.persistencia.PagamentoDao(app);

    pagamentoDao.lista(function(err, results) {
      
      if(err) {
        console.log(err);
        res.status(500).send(err);
        return;
      }

      console.log('All pagamentos (: ',JSON.stringify(results));
      res.json(results);
    });
  });

  app.get('/pagamentos/pagamento/:id', function(req, res) {
    //curl -X GET http://localhost:3000/pagamentos/pagamento/20 -v

    let id = req.params.id; //ID passando como paramentos na Url usando o curl
    console.log(`Consultando pagamento com id: ${id}`);
    logger.info(`Consultando pagamento com id ${id}`);

    let memcachedClient = app.services.memcachedClient();
        memcachedClient.get(`pagamento-${id}`, function(err, retorno) {

          if(err || !retorno) {
            
            console.log('MISS - chave não encontrada');
            
            let pagamentoDao = new app.persistencia.PagamentoDao(app);

            pagamentoDao.buscaPorId(id, function(err, results) {
              
              err ? (
                console.log(err),
                res.status(500).send(err)
              ) : (
                console.log(`Pagamento encontrado: ${JSON.stringify(results)}`),
                res.json(results)
              );
            });

          } else {
            console.log(`HIT - valor: ${JSON.stringify(retorno)}`);
            res.json(retorno);
          }
        });
  });

  app.post('/pagamentos/pagamento', [
    check('pagamento.forma_de_pagamento', 'A forma de pagamento eh obrigátoria').not().isEmpty(),
    check('pagamento.valor', 'O campo valor é obrigátorio e deve ser do tipo numerico').not().isEmpty().isFloat()
  ], function (req, res) {

    const erros = validationResult(req);

    if (!erros.isEmpty()) {
      console.log('Erros de validação foram encontrados');
      res.status(400).json({ erros: erros.array() });
      return;
    }

    let pagamento = req.body["pagamento"];
    pagamento.status = "criado";
    pagamento.data = new Date;

    let pagamentoDao = new app.persistencia.PagamentoDao(app);

    pagamentoDao.salva(pagamento, function (err, results) {

      pagamento.id = results.insertId;

      if (err) {
        console.log('Erro ao inserir no banco: ' + err);
        res.status(500).send(err);
        return;
      }

      let memcachedClient = app.services.memcachedClient();
          memcachedClient.set(`pagamento-${pagamento.id}`, pagamento, 60000, function(err) {

            if(err) {
              console.log('Erro ao salvar em cache');
            } else {
              console.log(`Nova chave adiciona ao cache: pagamento-${pagamento.id}`);
            }
          });

      if (pagamento.forma_de_pagamento == 'cartao') {

        let cartao = req.body["cartao"]
            ,clienteCartoes = new app.services.clienteCartoes();

        clienteCartoes.autoriza(cartao, function (erro, request, resposta, retorno) {

          if (erro) {
            console.log('Erro ao autorizar o cartão', erro);
            res.status(400).send(erro);
            return;
          }

          res.location(`/pagamentos/pagamento/${pagamento.id}`);

          //HATEOAS -: Hypermedia As The Engine Of Application State
          let response = {
            forma_de_pagamento: pagamento,
            cartao: retorno,
            links: [
              {
                href: `http://localhost:3000/pagamentos/pagamento/${pagamento.id}`,
                rel: 'Confirmar',
                method: 'PUT'
              },
              {
                href: `http://localhost:3000/pagamentos/pagamento/${pagamento.id}`,
                rel: 'Cancelar',
                method: 'DELETE'
              }
            ]
          };
          res.status(201).json(response);

        });

      } else {

        res.location(`/pagamentos/pagamento/${pagamento.id}`);

        //HATEOAS -: Hypermedia As The Engine Of Application State
        let response = {
          forma_de_pagamento: pagamento,
          links: [
            {
              href: `http://localhost:3000/pagamentos/pagamento/${pagamento.id}`,
              rel: 'Confirmar',
              method: 'PUT'
            },
            {
              href: `http://localhost:3000/pagamentos/pagamento/${pagamento.id}`,
              rel: 'Cancelar',
              method: 'DELETE'
            }
          ]
        };

        res.status(201).json(response);
      }

      console.log('Pagamento sendo processado');
    });
  });

  app.put('/pagamentos/pagamento/:id', function (req, res) {

    let pagamento = {};
    let id = req.params.id;

    pagamento.id = id;
    pagamento.status = "Confirmado";

    let pagamentoDao = new app.persistencia.PagamentoDao(app);

    pagamentoDao.atualiza(pagamento, function (err, results) {
      err ? (
        console.log('Erro ao confirmar pagamento: ' + err), res.status(500).send(err)
      ) : (
          res.send(pagamento), console.log('Pagamento confirmado.')
        );
    });

  });

  app.delete('/pagamentos/pagamento/:id', function (req, res) {
    //Seta o status do pagamento como 'Cancelado', mais não exclui do banco

    let pagamento = {};
    let id = req.params.id;

    pagamento.id = id;
    pagamento.status = "Cancelado";

    let pagamentoDao = new app.persistencia.PagamentoDao(app);

    pagamentoDao.atualiza(pagamento, function (err, results) {
      err ? (
        console.log('Erro ao cancelar o pagamento: ' + err), res.status(500).send(err)
      ) : (
          console.log('Pagamento cancelado com sucesso.'), res.status(204).send(pagamento)
        );
    });
  });
}