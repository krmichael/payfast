const winston = require('winston');
const fs = require('fs');

//Verificando/criando a pasta logs caso ela não exista
if(!fs.existsSync('./app/logs')) {
  fs.mkdirSync('./app/logs');
}

module.exports = winston.createLogger({
  transports: [
    new winston.transports.File({
      level: 'info',
      filename: './app/logs/payfast.log',
      maxsize: 100000,
      maxFiles: 10
    })
  ]
});