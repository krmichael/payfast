//memcached -vv no terminal para subir o memcached
const memcached = require('memcached');

function createMemcachedClient() {

  const client = new memcached('localhost:11211', {
    retries: 10, //qtd de tentativas feitas por request em busca do item em cache
    retry: 10000, //tempo que ele vai esperar para tentar novamente buscar o item em cache
    remove: true //remove o item do cache caso ele não o encontre, seguindo as regras acima
  });

  return client;
}

module.exports = function() {
  return createMemcachedClient;
}

/* client.set('pagamento-20', { "id": 20 }, 60000, function(err) {
  
  console.log('Nova chave adicionada ao cache: pagamento-20');
});

client.get('pagamento-20', function(err, result) {

  if(err || !result) {
    console.log('MISS - chave não encontrada');
  } else {
    console.log(`HIT - valor: ${JSON.stringify(result)}`);
  }
}); */