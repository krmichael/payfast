const mysql = require('mysql2');
let pool = null;

function _createPool() {

  if(!process.env.NODE_ENV) {
    pool = mysql.createPool({
      connectionLimit: 100,
      host: 'localhost',
      user: 'root',
      password: 'silva!$!$',
      database: 'payfast'
    });
  }

  if(process.env.NODE_ENV == 'production') {
    let url = process.env.CLEARDB_DATABASE_URL;
    let grup = url.match(/mysql:\/\/(.*):(.*)@(.*)\/(.*)\?reconnect=true/);

    pool = mysql.createPool({
      connectionLimit: 10,
      host: grup[3],
      user: grup[1],
      password: grup[2],
      database: grup[4]
    });
  }

  pool.on('enqueue', function() {
    console.log('Aguardando conexão...');
  });
}
_createPool();


function createDBConnection(callback) {
  
  return pool.getConnection(function(err, connection) {

    if(err) {
      console.log('Erro ao obter o pool (: ',err);

      pool.end(function(err) {

        if(err) {
          console.log('Erro ao fechar o pool (: ',err);
        }

        _createPool();
      });

      return;
    }

    return callback(null, connection);
  });
}

module.exports = function() {
  return createDBConnection;
}