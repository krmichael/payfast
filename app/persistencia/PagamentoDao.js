function PagamentoDao(connection) {
  this._connection = connection;
}

PagamentoDao.prototype.lista = function(callback) {

  this._connection.persistencia.connectionFactory(function(err, connection) {
    
    if(err) {
      console.log('Sorry! Error Internal, Watting a moment please and try again (: ');
      return;
    }

    connection.query('select * from pagamentos', function(err, results) {
      connection.release();
      callback(err, results);
    });
  });
}

PagamentoDao.prototype.salva = function(pagamento, callback) {

  this._connection.persistencia.connectionFactory(function(err, connection) {

    if(err) {
      console.log('Sorry! Error Internal, Watting a moment please and try again (: ');
      return;
    }

    connection.query('insert into pagamentos set ? ', pagamento, function(err, results) {
      connection.release();
      callback(err, results);
    });
  });
}

PagamentoDao.prototype.atualiza = function(pagamento, callback) {

  this._connection.persistencia.connectionFactory(function(err, connection) {

    if(err) {
      console.log('Sorry! Error Internal, Watting a moment please and try again (: ');
      return;
    }

    connection.query('update pagamentos set status = ? where id = ?',
      [pagamento.status, pagamento.id], function(err, results) {
        connection.release();
        callback(err, results);
      });
  });
}

PagamentoDao.prototype.buscaPorId = function(id, callback) {
  
  this._connection.persistencia.connectionFactory(function(err, connection) {

    if(err) {
      console.log('Sorry! Error Internal, Watting a moment please and try again (: ');
      return;
    }

    connection.query('select * from pagamentos where id = ?',
      [id], function(err, results) {
        connection.release();
        callback(err, results);
      });
  });
}

module.exports = function() {
  return PagamentoDao;
}