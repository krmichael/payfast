const FS = require('fs');

FS.readFile('cachoeira.jpg', (err, buffer) => {

  err ? console.log(`Erro ao ler o arquivo: ${err}`) :
        console.log('Leitura completa!');

  FS.writeFile(`cachoeira${Date.now()}.jpg`, buffer, err => {

    err ? console.log(`Erro ao escrever o arquivo ${err}`) :
          console.log('Escrita completa!')});
});