const FS = require('fs');

FS.createReadStream('cachoeira.jpg')
  .pipe(FS.createWriteStream(`cachoeira-${Date.now()}-stream.jpg`))
  .on('finish', () => console.log('Arquivo escrito com stream'));