const app = require('./config/express')();

const hostname = '0.0.0.0';
const port = process.env.PORT || 3000;

app.listen(port, hostname, function() {
  console.log(`Server running at http://${hostname}:${port}`);
});