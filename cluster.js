const cluster = require('cluster')
      ,os = require('os')
      ,cpus = os.cpus();

cluster.isMaster ? (
  
  cpus.forEach(() => cluster.fork()),

  cluster.on('listening', worker => console.log(`cluster conectado com id ${worker.process.pid}`)),

  cluster.on('exit', worker => {
    console.log(`cluster ${worker.process.pid} desconectado`);
    cluster.fork();
  })

) : (
  require('./index.js')
);